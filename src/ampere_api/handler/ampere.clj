(ns ampere-api.handler.ampere
  (:require [ampere-api.boundary.ampere :as b]
            [ampere-api.boundary.ampere.user :as user]
            [ampere-api.boundary.core :as c]
            [ampere-api.boundary.util :as u]
            [ataraxy.response :as response]
            [buddy.auth :as buddy]
            [duct.logger :refer [log]]
            [integrant.core :as ig]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.util.response :as resp]
            [ring.util.io :as ring-io]
            [ampere-api.boundary.ampere.db :as db]))

;; ## Handlers for adempiere REST-like json web api
(defonce last-request (atom {}))

(defn create! [ampere ctx table m]
 (try
   (if-let [po (c/create! ampere {:ctx ctx
                                  :table table
                                  :data m})]
     [::response/created (str "/m_product/" (:m_product_id po)) po]
     [::response/internal-server-error {:error :not-saved}])
   (catch Exception e [::response/internal-server-error {:error (.getMessage e)
                                                         :data (ex-data e)} ])))

;; Create invoked via POST to collection endpoint ./api/table_name
(defmethod ig/init-key ::create [_ {:keys [:ampere]}]
  (fn [{[_ table m] :ataraxy/result
        ctx :ctx
        :as req}]
    (create! ampere ctx table m)))

;; Update a record with PATCH to resource ./api/table_name/id
;; We don't support PUT for update as we don't guarantee idempotence
(defmethod ig/init-key ::update [_ {:keys [:ampere]}]
  (fn [{[_ table id m] :ataraxy/result
        ctx :ctx
        :as req}]
    (if-let [po (c/update! ampere {:ctx ctx
                               :table table
                               :id id
                               :data m})]
      [::response/ok {:result id}])))

;; Delete a record with DELETE to resource ./api/table_name/id
(defmethod ig/init-key ::delete [_ {:keys [:ampere]}]
  (fn [{[_ table id] :ataraxy/result
        ctx :ctx :as req}]
    (if-let [po (c/delete! ampere {:ctx ctx
                               :table table
                                      :id (u/coerce-to Integer id)})]
      [::response/ok {:result id}])))

;; List members of the collection with GET on ./api/table_name
(defmethod ig/init-key ::list [_ {:keys [ampere]}]
  (fn [{[_ table body-params {:strs [fields where]}] :ataraxy/result
        ctx :ctx}]
    [::response/ok
     {:result (c/query ampere {:ctx ctx
                                     :table table
                                     :fields fields
                                     :rql-where where})}]))

(defn json-query-handler
  [ampere ctx table rql]
  (if-let [result (:result
                   (c/query-raw ampere
                                {:ctx ctx
                                 :table table
                                 :rql-where rql}))]
    [::response/ok {:result result}]
    [::response/not-found {:error :not-found}]))

(defn csv-query-handler
  [ampere ctx table rql]
    (-> (resp/response
         (ring-io/piped-input-stream
          (c/query-csv ampere
                       {:ctx ctx
                        :table table
                        :rql-where rql})))
         (resp/content-type "text/csv")
         (resp/header "Content-Disposition" (str "attachment; filename="
                                                 table ".csv"))))

(defn raw-query-handler [ampere [_ table {b-where :where}
                                 {q-where "where"
                                  format "format"}]
                         ctx]
  (let [rql-where (or b-where q-where)]
    (if (= format "csv")
      (csv-query-handler ampere ctx table rql-where)
      (json-query-handler ampere ctx table rql-where))))

;; Query database directly with GET on ./api/table_name/raw/
(defmethod ig/init-key ::query-raw [_ {:keys [ampere]}]
  (fn [{params :ataraxy/result
        ctx :ctx}]
    (raw-query-handler ampere params ctx)))

(defn get-by-id
  [ampere table id fields ctx]
  (try
    (if-let [po (c/get-by-id ampere {:ctx ctx
                                     :table table
                                     :fields fields
                                     :id (u/coerce-to Integer id)})]
      [::response/ok {:result po}]
      [::response/not-found {:error :not-found}])
    (catch Exception e [::response/internal-server-error e])))

;; Retrieve a single record with GET on ./api/table_name/id
(defmethod ig/init-key ::get [_ {:keys [ampere]}]
  (fn [{[_ table id body-params {:strs [fields]}] :ataraxy/result
        ctx :ctx}]
    (if (nil? id)
      [::response/bad-request {:error "id required"}]
      (get-by-id ampere table id fields ctx))))

(defn wrap-cors-origin
  [handler allowed-origins]
  (wrap-cors handler
             :access-control-allow-origin [(re-pattern allowed-origins)]
             :access-control-allow-methods [:get :put :patch :post :delete] ))

;; Wrap requests with CORS header
(defmethod ig/init-key ::wrap-cors
  [_ {:keys [allowed-origins]}]
  (fn [handler]
    (wrap-cors-origin handler allowed-origins)))

(defn wrap-role
  "Add Ampere context to request based on auth token data"
  [handler {:keys [logger] :as options}]
  (fn [{{:keys [preferred_username]} :identity
        :as request}]
      (log logger :info ::wrap-role-info {:request request}  )
      (if-not (buddy/authenticated? request)
        [::response/unauthorized {:error "Valid auth token required to access API"}]
        (try
          (let [ctx (user/create-user-ctx preferred_username)]
              (handler (reset! last-request (-> request
                                                (assoc :ctx ctx)))))
          (catch Exception e [::response/unauthorized {:error (or (.getMessage e) "Unknown cause")
                                                       :username preferred_username}]))
        )))

(defmethod ig/init-key ::wrap-role
  [_ options]
  (fn [handler]
    (wrap-role handler options)))
