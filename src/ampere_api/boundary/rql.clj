(ns ampere-api.boundary.rql
  (:require [clojure.string :as str]
            [coercer.core :as coerce]
            [instaparse.core :as insta]
            [ring.util.codec :as cd]
            [ampere-api.boundary.util :as u]))

;; RQL query language from http://persvr.org/rql/
;; based on draft grammar retrieved from https://dundalek.com/rql/draft-zyp-rql-00.html
(def rql
  "
  query = and

  and = operator *( <\"&\"> operator )
  <operator> = comparison / call-operator / group
  call-operator = name <\"(\"> [ argument *( <\",\"> argument ) ] <\")\">
  <argument> = call-operator / value
  value = *nchar / typed-value / array
  typed-value = 1*nchar \":\" *nchar
  array = \"(\" [ value *( \",\" value ) ] \")\"
  name = *nchar

  comparison = name ( <\"=\"> [ name <\"=\"> ] ) value
  group = \"(\" ( and / or ) \")\"
  or = operator *( \"|\" operator )

  <nchar> = unreserved / pct-encoded / \"*\" / \"+\"
  pct-encoded   = \"%\" HEXDIG HEXDIG
  <unreserved>    = ALPHA / DIGIT / \"-\" / \".\" / \"_\" / \"~\"
  ")

(def rql-parser
  (insta/parser
   rql
   :input-format :abnf))

(defn compare-op [ctx op f v]
  (let [fields (or (:fields @ctx) {})] 
    (if (contains? fields (str/lower-case f))
      (let [t (get fields (str/lower-case f))]
        (swap! ctx update-in [:params] conj (u/coerce-to t v))
        (str f " " op " ?"))
      (do 
        (swap! ctx update-in [:errors] conj (str "Invalid field: " f))
        nil))))

(defmulti call-operator
  (fn [_ & args] (first args)))
(defmethod call-operator "and" [_ _ & args]
  (str/join " and " args))
(defmethod call-operator "or" [_ _ & args]
  (str "(" (str/join " or " args) ")"))
(defmethod call-operator "eq" [ctx _ & args]
  (compare-op ctx "=" (first args) (second args)))
(defmethod call-operator "lt" [ctx _ & args]
  (compare-op ctx "<" (first args) (second args)))
(defmethod call-operator "le" [ctx _ & args]
  (compare-op ctx "<=" (first args) (second args)))
(defmethod call-operator "gt" [ctx _ & args]
  (compare-op ctx ">" (first args) (second args)))
(defmethod call-operator "ge" [ctx _ & args]
  (compare-op ctx ">=" (first args) (second args)))
(defmethod call-operator "ne" [ctx _ & args]
  (compare-op ctx "<>" (first args) (second args)))

(defn comparison
  ([ctx n v]
   (call-operator ctx "eq" n v))
  ([ctx n o v]
   (call-operator ctx o n v)))

(defn query [ctx s]
  (-> (select-keys @ctx [:params :errors])
      (assoc :query s)))

(defn q->sqlvec [{:keys [query params errors]}]
  (when query
    (if errors
      errors
      (apply vector query params))))

(defn pct-decode [& args]
  (cd/percent-decode (apply str args)))

(defn rql->sql
  [s ctx]
  (when (and s ctx)
    (->> s
         rql-parser
         (insta/transform {:query (partial query ctx)
                           :call-operator (partial call-operator ctx)
                           :comparison (partial comparison ctx)
                           :and (partial call-operator ctx "and")
                           :group str
                           :ALPHA str
                           :DIGIT str
                           :HEXDIG str
                           :value str
                           :name str
                           :pct-encoded pct-decode}))))
