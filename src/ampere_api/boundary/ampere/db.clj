(ns ampere-api.boundary.ampere.db
  (:require [clojure.walk :as walk]
            [cheshire.core :as json]
            [hugsql.adapter.next-jdbc :as adapter]
            [hugsql.core :as hugsql]
            [integrant.core :as ig]
            [next.jdbc :as jdbc]
            [next.jdbc.date-time :as jdt]
            [next.jdbc.prepare :as prepare]
            [next.jdbc.result-set :as rs]
            [java-time :as jt]
            [clojure.java.io :as io]
            [ring.util.io :as ring-io])
  (:import java.sql.PreparedStatement
           [org.compiere.util DB DisplayType]
           [org.compiere.db CConnection]
           org.postgresql.util.PGobject
           [org.postgresql.copy CopyManager]))

(hugsql/def-db-fns "ampere_api/ampere.sql" {:quoting :ansi
                                       :adapter (adapter/hugsql-adapter-next-jdbc)})
(hugsql/def-sqlvec-fns "ampere_api/ampere.sql" {:quoting :ansi
                                         :adapter (adapter/hugsql-adapter-next-jdbc)})

;;;; add json support to postgresql adaptor
;;;; taken from next.jdbc wiki
(defn ->pgobject
  "Transforms Clojure data to a PGobject that contains the data as
  JSON. PGObject type defaults to `jsonb` but can be changed via
  metadata key `:pgtype`"
  [x]
  (let [pgtype (or (:pgtype (meta x)) "jsonb")]
    (doto (PGobject.)
      (.setType pgtype)
      (.setValue (json/generate-string x)))))

(defn <-pgobject
  "Transform PGobject containing `json` or `jsonb` value to Clojure
  data."
  [^org.postgresql.util.PGobject v]
  (let [type  (.getType v)
        value (.getValue v)]
    (if (#{"jsonb" "json"} type)
      (when value
        (with-meta (json/parse-string value ) {:pgtype type}))
          value)))

;; if a SQL parameter is a Clojure hash map or vector, it'll be transformed
;; to a PGobject for JSON/JSONB:
(extend-protocol prepare/SettableParameter
  clojure.lang.IPersistentMap
  (set-parameter [m ^PreparedStatement s i]
    (.setObject s i (->pgobject m)))

  clojure.lang.IPersistentVector
  (set-parameter [v ^PreparedStatement s i]
    (.setObject s i (->pgobject v))))

;; if a row contains a PGobject then we'll convert them to Clojure data
;; while reading (if column is either "json" or "jsonb" type):
(extend-protocol rs/ReadableColumn
  PGobject
  (read-column-by-label [^org.postgresql.util.PGobject v _]
    (<-pgobject v))
  (read-column-by-index [^org.postgresql.util.PGobject v _2 _3]
    (<-pgobject v)))

(defn get-conn
  "Get ampere pooled database read connection"
  []
  (DB/getConnectionRO))

(defn get-driver-conn
  "Get raw driver database connection"
  []
  (let [conn (CConnection/get nil)
        db (.getDatabase conn)]
    (.getDriverConnection db conn)))

(defn get-json-data [sql]
  (with-open [c (get-conn)]
    (get-table-data-json c {:sql [sql]})))

(defn stream-csv [sql os]
    (with-open [c (get-driver-conn)]
      (let [cm (CopyManager. c)
            sql (first (get-csv-sqlvec {:sql [sql]}))]
        (.copyOut cm sql os))))

