(ns ampere-api.boundary.ampere.process
  (:require [ampere-api.boundary.core :as core]
            [ampere-api.boundary.ampere.core :as ac]
            [ampere-api.boundary.ampere.model :as model])
  (:import [ampere_api.boundary.ampere.core Ampere]))

(defn run-process! [ampere args]
  (str args))

(defn get-muser
  [ctx username]
  )

(defn get-process [ampere {:keys [ctx value]}]
  (let [process-table (ac/get-mtable ctx "AD_Process")]
    (.getPO process-table (str "IsActive='Y' AND Value='" value "'") nil)))

(defn get-pinstance [ampere args]
  (str args))

(defn get-pinstances [ampere args]
  (str args))

(extend-protocol core/ReportAndProcess
  Ampere
  (run-process! [this args]
    (run-process! args))
  (get-process [this args]
    (get-process args))
  (get-pinstance [this args]
    (get-pinstance args))
  (get-pinstances [this args]
    (get-pinstances args)))
