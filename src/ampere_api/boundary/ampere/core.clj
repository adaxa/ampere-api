(ns ampere-api.boundary.ampere.core
  (:import [org.compiere.model MTable Query POInfo]
           org.compiere.util.DisplayType))


(defrecord Ampere [started])

;; Start ampere system
;; Requires map containing database :host :port :name :user :password
(defn start-ampere
  "Starts up Ampere environment in server mode, establishing
   database pool connection."
  [db]
    (System/setProperty "db.name" (:name db))
    (System/setProperty "db.host" (:host db))
    (System/setProperty "db.port" (:port db))
    (System/setProperty "db.user" (:user db))
    (System/setProperty "db.password" (:password db))
    (org.compiere.Adempiere/startup false))

;; Load table model from ampere, returns an MTable
(defn get-mtable
  [ctx table-name]
  (MTable/get ctx table-name))
