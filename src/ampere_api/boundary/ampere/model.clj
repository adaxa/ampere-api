(ns ampere-api.boundary.ampere.model
  (:require [ampere-api.boundary.ampere.user :as user
             :refer [can-read-table?]]
            [ampere-api.boundary.ampere.core :refer [get-mtable]]
            [ampere-api.boundary.util :as u]
            [clojure.string :as str]
            [ampere-api.boundary.core :as core]
            [ampere-api.boundary.ampere.db :as db]
            [ampere-api.boundary.rql :as r])
  (:import [org.compiere.model Query POInfo]
           [ampere_api.boundary.ampere.core Ampere]
           [org.compiere.util DisplayType]
           java.util.Properties))

(declare create-!)



(defn get-field-types
  "Returns map of column names to java class for table"
  [ctx table-name]
  (let [t (get-mtable ctx table-name)
        mcs (.getColumns t false)]
    (reduce (fn [m c] (assoc m (-> c
                                   .getColumnName
                                   str/lower-case)
                             (-> c
                                 .getAD_Reference_ID
                                 (DisplayType/getClass true)))) {} mcs)))

(defn create-query
  "Create a Query object for retrieving ampere data"
  [ctx table trx where-sqlvec]
  (when-let [query (let [mt (get-mtable ctx table)]
                (if (can-read-table? ctx mt)
                  (Query. ctx mt (first where-sqlvec) trx)
                  (throw (ex-info "Not permitted to access table"
                                  {:table table
                                   :ctx ctx}))))]
    (-> query
        (.setApplyAccessFilter  true false)
        (.setParameters (rest where-sqlvec)))))

(defn list-pos
  "Retrieve array of POs (persistent objects) from ampere table named table filtered by
  where SQL string, using the optional trx name."
  [ctx table where trx]
  (-> (create-query ctx table where trx)
      (.list)))

(defn set-po-value!
  "Update PO field with new value."
  [po col val]
  (println (str po col val))
  (let [colname (name col)
        colidx (.get_ColumnIndex po colname)
        poinfo (POInfo/getPOInfo (Properties.) (.get_Table_ID po))
        cc (.getColumnClass poinfo colidx)
        ok (cond
          (nil? val) (.set_ValueOfColumn po colname val)
          :else (.set_ValueOfColumn
                 po colname (u/coerce-to cc val)))]
    (if (not true)
      (throw (ex-info "Unable to set PO value" {:column col :value val}))
      ok)))

(defn- resolve-field
  "For field i in PO resolve any lookup display value and add to map"
  [po i]
  (let [columnName (.get_ColumnName po i)
        poinfo (POInfo/getPOInfo (java.util.Properties.) (.get_Table_ID po))
        lookup (.getColumnLookup poinfo i)
        value  (.get_Value po i)
        displayValue (if lookup
                       (.getDisplay lookup value)
                       value)]
    (into {}
          (cond-> [[(keyword (str/lower-case columnName)) value]]
             lookup (conj [(keyword (str (str/lower-case columnName) "_display")) displayValue])))))

(defn query-table-po
  "Query data from ampere"
  [{:keys [ctx table trx where] :or {where ["false"]}}]
  (list-pos ctx table trx where))

(defn query-table-po-by-id-
  [{:keys [id kc] :as args}]
  (let [where [(str kc " = ? ") id]]
    (first (query-table-po (assoc args :where where)))))

(defn get-po-by-id-
  [{:keys [table ctx] :or {ctx (Properties.)} :as args}]
  (when-let [t (get-mtable ctx table)]
    (let [kc (when (.isSingleKey t) (first (.getKeyColumns t)))]
      (query-table-po-by-id- (assoc args :kc kc)))))

(defn po->map
  "Convert PO to map, optionally restricting keys to returned fields"
  ([po] (po->map po nil))
  ([po fields]
   (into {}
         (for [i (range (.get_ColumnCount po))]
           (let [columnName (.get_ColumnName po i)]
             (when (or (not fields)
                       (some #(= (str/lower-case columnName) (str/lower-case %)) fields))
               (resolve-field po i)))))))

(defn query-table-
  "Query data from ampere and convert to list of maps"
  [{:keys [ctx table trx where fields] :or {where ["false"]}}]
  (doall
   (for [x (iterator-seq (.iterator (list-pos ctx table trx where)))]
     (po->map x fields))))

(defn raw-query-table-
  "Query data from ampere and convert to list of maps"
  [{:keys [ctx table where] :or {where ["false"]}}]
  (let [query (create-query ctx table (first where) nil)
        sql (.getSQL query)]
    (db/get-json-data sql)))

(defn csv-query-table-
  "Return function to stream csv file to output stream"
  [{:keys [ctx table where] :or {where ["false"]}}]
  (fn [os]
    (let [query (create-query ctx table (first where) nil)
          sql (.getSQL query)]
      (db/stream-csv sql os))))

(defn query-table-by-id-
  [{:keys [id kc] :as args}]
  (let [where [(str kc " = ? ") id]]
    (first (query-table- (assoc args :where where)))))

(defn get-by-id-
  [{:keys [table ctx] :or {ctx (Properties.)} :as args}]
   (when-let [t (get-mtable ctx table)]
     (let [kc (when (.isSingleKey t) (first (.getKeyColumns t)))]
       (query-table-by-id- (assoc args :kc kc)))))

(defn get-new-po
  "Create a new PO"
  [{:keys [table ctx trx] :or {ctx (Properties.)}}]
    (if-let [t (get-mtable ctx table)]
      (.getPO t (Integer. 0) trx)
      (throw (ex-info "Unable to create PO" {:table table}))))

(defn- update-po!
  [po {:keys [table id data] :as args}]
  (if (not (user/can-update-po? po))
    (throw (ex-info "Not permitted to update PO" {:table table :id id :data data}))
    (do (doall
         (for [[col val] data]
           (set-po-value! po col val)))
        (.saveEx po)
        po)))

(defn- create-child!
  [parent-po ctx child]
  (let [parent-id (.get_ID parent-po)
        parent-col (-> parent-po
                       .get_KeyColumns
                       first
                       (str/lower-case)
                       keyword)
        cm (create-!
            (-> child
                (assoc-in
                 [:data parent-col] parent-id)
                (assoc :ctx ctx)))]
    (assoc child :data cm)))

(defn- create-!
  [{:keys [data ctx] :as args}]
  (let [children (:children data)
        po (get-new-po args)
        pm (-> po
               (update-po! (assoc args :data (dissoc data :children)))
               po->map)
        cms (map #(create-child! po ctx %) children)]
    (cond-> pm
      (seq cms) (assoc :children cms))))

(defn- update-!
  [{:keys [table id] :as args}]
  (let [po (get-po-by-id- args)]
      (if (nil? po)
        (throw (ex-info "Unable to load PO for update" {:table table :id id}))
        (-> po
            (update-po! args)
            po->map))))

(defn query-raw [rql-where table args]
  (let [opts (atom {:params []
                    :fields (get-field-types (Properties.) table)})
        sql-where (-> rql-where
                      (r/rql->sql opts)
                      r/q->sqlvec)]
    (raw-query-table- (-> args
                          (assoc :where (or sql-where ["true"]))))))

(defn query-csv [rql-where table args]
  (let [opts (atom {:params []
                    :fields (get-field-types (Properties.) table)})
        sql-where (-> rql-where
                      (r/rql->sql opts)
                      r/q->sqlvec)]
    (csv-query-table- (-> args
                          (assoc :where (or sql-where ["true"]))))))


(defn- delete-!
  [args table id]
  (let [po (get-po-by-id- args)]
    (if (nil? po)
      (throw (ex-info "Unable to load PO for delete" {:table table :id id}))
      (if (not (user/can-update-po? po))
        (throw (ex-info "Not permitted to delete PO" {:table table :id id :po po}))
        (.deleteEx po false)))))

(extend-protocol core/Model
  Ampere
  (create!
    [this args]
    (create-! args))

  ;;  "Retrieve single PO (persistent object) from ampere table for given id
  ;;  as a map, with optional trx name. Only works for single-key
  ;; tables."
  (get-by-id
    [this args]
    (get-by-id- args))

  ;; Query ampere data via PO interface returning a list of maps representing the PO
  (query [this {:keys [rql-where table] :as args}]
    (let [opts (atom {:params []
                      :fields (get-field-types (Properties.) table)})
          sql-where (-> rql-where
                        (r/rql->sql opts)
                        r/q->sqlvec)]
      (query-table- (-> args
                        (assoc :where sql-where)))))

  (query-raw [this {:keys [rql-where table] :as args}]
    (query-raw rql-where table args))

  (query-csv [this {:keys [rql-where table] :as args}]
    (query-csv rql-where table args))

  ;;  "Update and return an ampere persistent object as a map."
  (update!
    [this args]
    (update-! args))

  ;;  "Delete an ampere persistent object with given id. Returns nil"
  (delete!
    [this {:keys [table id] :as args}]
    (delete-! args table id)))
