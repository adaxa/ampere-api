(ns ampere-api.boundary.ampere.user
  (:require [ampere-api.boundary.ampere.core :refer [get-mtable]])
  (:import java.util.Properties
           [org.compiere.model MRole MTable POInfo Query]
           [org.compiere.util DisplayType Env Trx]))

; Load user from ampere, returns an MUser
(defn get-muser
  [ctx username]
  (let [user-table (get-mtable ctx "AD_User")]
    (.getPO user-table (str "IsActive='Y' AND Value='" username "'") nil)))

; Create an ampere context for the specified user
(defn create-user-ctx
  [username]
  (let [ctx (Properties.)]
    (if-let [user (get-muser ctx username)]
      (let [client (.getAD_Client_ID user)
            org (.getAD_Org_ID user)]
        (if-let [roles (.getRoles user org)]
          (if-let [role (first roles)]
            (do (.setProperty ctx "#AD_Client_ID" (str client))
                (.setProperty ctx "#AD_User_ID" (str (.getAD_User_ID user)))
                (.setProperty ctx "#AD_Org_ID" (str org))
                (.setProperty ctx "#AD_Role_ID" (str (.getAD_Role_ID role)))
                ctx)
            (throw (ex-info "No roles for user" {:user username})))))
      (throw (ex-info "Invalid user" {:user username})))))

(defn can-read-table?
  "Check if current user (as defined in supplied ctx) is permitted to read from MTable t"
  [ctx t]
  (let [role (MRole/get ctx
                        (Env/getAD_Role_ID ctx)
                        (Env/getAD_User_ID ctx)
                        false)]
    (.isTableAccess role (.getAD_Table_ID t) true)))

(defn can-update-po?
  "Check if current user (as defined in PO ctx) is permitted to update PO"
  [po]
  (let [ctx (.getCtx po)
        role (MRole/get ctx
                        (Env/getAD_Role_ID ctx)
                        (Env/getAD_User_ID ctx)
                        false)]
    (.canUpdate role
                (.getAD_Client_ID po)
                (.getAD_Org_ID po)
                (.get_Table_ID po)
                (.get_ID po)
                true)))

(defn can-run-process?
  "Check if current user (as defined in supplied ctx) is permitted to run process"
  [ctx process-id]
  (let [role (MRole/get ctx
                        (Env/getAD_Role_ID ctx)
                        (Env/getAD_User_ID ctx)
                        false)]
    (.getProcessAccess role process-id)))
