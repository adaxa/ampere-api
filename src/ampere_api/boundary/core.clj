(ns ampere-api.boundary.core)

(defprotocol Model
  (create! [this args])
  (get-by-id [this args])
  (query [this args])
  (query-raw [this args])
  (query-csv [this args])
  (update! [this args])
  (delete! [this args]))

(defprotocol ReportAndProcess
  (run-process! [this args])
  (get-process [this args])
  (get-pinstance [this args])
  (get-pinstances [this args]))
