(ns ampere-api.boundary.ampere
  (:require [ampere-api.boundary.ampere.core :as core]
            [integrant.core :as ig]))

(require 'ampere-api.boundary.ampere.model)

;; initiate the ampere component
(defmethod ig/init-key ::ampere [_ db-params]
  (-> db-params
      core/start-ampere
      core/->Ampere
      (assoc :params db-params)))

