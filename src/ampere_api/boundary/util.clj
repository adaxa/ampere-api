(ns ampere-api.boundary.util
  (:import java.sql.Timestamp
           [java.time.format DateTimeFormatterBuilder DateTimeParseException]
           [java.time  LocalDateTime Instant]
           java.time.temporal.ChronoField))

(defn parse-timestamp
  "Attempt to parse string to timestamp, supports DateTimeFormatter.ISO_INSTANT
  (yyyy-MM-dd'T'HH:mm:ss'Z') format and JDBC (yyyy-MM-dd HH:mm:ss) format.
  Throws exception on failed JDBC parse."
  [v]
  (or
   (try (-> v
            Instant/parse
            Timestamp/from)
        (catch DateTimeParseException _ nil))
   (-> v
       (LocalDateTime/parse 
        (-> (DateTimeFormatterBuilder.)
            (.appendPattern "yyyy-MM-dd HH:mm:ss")
            (.appendFraction (ChronoField/NANO_OF_SECOND) 0 9 true)
            (.toFormatter)))
       Timestamp/valueOf)))

(defmulti coerce-to
  "Function to convert java value to expected type t"
  (fn [t _] t))
(defmethod coerce-to Integer [_ v]
  (cond (instance? String v) (Integer/parseInt v)
        :else (int v)))
(defmethod coerce-to Boolean [_ v]
  (cond (instance? String v) (or (= "Y" v) (= "true" v))
        :else v))
(defmethod coerce-to BigDecimal [_ v]
  (cond (instance? BigDecimal v) v
        :else (BigDecimal. v)))
(defmethod coerce-to Timestamp [_ v]
  (cond (instance? String v)
        (parse-timestamp v)
        :else v))
(defmethod coerce-to Instant [_ v]
  (cond (instance? String v) (Instant/parse v)
        :else v))
(defmethod coerce-to :default [_ v] v)
