(ns ampere-api.boundary.ampere.model-test
  (:require [ampere-api.boundary.ampere :as a]
            [ampere-api.boundary.ampere.model :as model]
            [ampere-api.boundary.ampere.user :as user]
            [ampere-api.boundary.core :as c]
            [ampere-api.boundary.ampere-test :as at]
            [clojure.string :as str]
            [clojure.test :as t]
            [integrant.core :as ig]))

(require 'matcher-combinators.test)

(t/deftest  ^:integration query-test
  (t/testing "read ampere data"
    (let [config (ig/init at/dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")]
      (t/is (str/starts-with? (.getSQL (model/create-query ctx "ad_client" nil ["ad_client_id=?" 0M])) "SELECT "))
      (t/is (= "System" (c/get-by-id ampere {:ctx ctx
                                                :table "test"
                                                :trx nil
                                                :id (Integer. 0)})))
      (t/is (= "System" (.getName (first
                                   (model/list-pos ctx "ad_client" nil ["AD_Client_ID=?" 0M])))))
      (t/is (= "System" (:name (c/get-by-id ampere {:ctx ctx
                                                          :table "ad_client"
                                                          :trx nil
                                                       :id (Integer. 0)}))))
      )))

(t/deftest  ^:integration raw-query-test
  (t/testing "read ampere data"
    (let [config (ig/init at/dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")]
      (t/is (= "System" (c/query-raw ampere {:ctx ctx
                                             :table "test"
                                             :rql-where "eq(ad_client_id,0)"})))
      )))

(t/deftest ^:integration query-test2
  (t/testing "Query using rql where and field list"
    (let [config (ig/init at/dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")]
      (t/is (match? {:value "SYSTEM",
                :isactive true,
                :ad_org_id 0,
                :ad_org_id_display "*"}
               (first (c/query ampere {:ctx ctx
                                             :table "ad_client"
                                             :rql-where "eq(ad_client_id,0)"
                                             :trx nil
                                             :fields ["ad_org_id" "isactive" "Value"]
                                             })))))))
