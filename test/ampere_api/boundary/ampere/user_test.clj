(ns ampere-api.boundary.ampere.user-test
  (:require [ampere-api.boundary.ampere :as a]
            [ampere-api.boundary.ampere.model :as model]
            [ampere-api.boundary.ampere.user :as user]
            [ampere-api.boundary.core :as c]
            [ampere-api.boundary.ampere-test :as at]
            [clojure.test :as t]
            [integrant.core :as ig]
            [ampere-api.boundary.ampere.core :as ac]))

(t/deftest  ^:integration query-test
  (t/testing "read ampere data"
    (let [config (ig/init at/dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")
          table (ac/get-mtable ctx "AD_Process")]
      (t/is (= {"#AD_Role_ID" "0", "#AD_User_ID" "0", "#AD_Client_ID" "0",
                "#AD_Org_ID" "0"} ctx))
      (t/is (user/can-read-table? ctx table))
      (t/is (not (user/can-run-process? ctx 100))))))
