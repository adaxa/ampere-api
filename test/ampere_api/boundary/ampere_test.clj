(ns ampere-api.boundary.ampere-test
  (:require [ampere-api.boundary.ampere :as a]
            [ampere-api.boundary.ampere.user :as user]
            [ampere-api.boundary.core :as c]
            [ampere-api.boundary.util :as u]
            [clojure.test :as t]
            [duct.core :as duct]
            [integrant.core :as ig])
  (:import java.sql.Timestamp
           org.compiere.util.DB))

(require 'matcher-combinators.test)

(def ctx (atom nil))

(def dev-config
  (-> (duct/resource "dev.edn")
      (duct/read-config)
      (select-keys [::a/ampere])))

(t/deftest ^:integration connection-test
  (t/testing "connection to ampere db"
    (let [conf (ig/init dev-config)]
      (t/is (DB/isConnected))
      (let [ctx (user/create-user-ctx "system")]
        (t/is (= "0" (get ctx "#AD_Client_ID")))))))

(t/deftest ^:integration query-nofields
  (t/testing "Query using rql where and field list"
    (let [config (ig/init dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")]
      (t/is (match? {:value "SYSTEM",
                :isactive true,
                :ad_org_id 0,
                :ad_org_id_display "*"}
               (first (c/query ampere {:ctx ctx
                                             :table "ad_client"
                                             :rql-where "eq(isactive,Y)"})))))))

(t/deftest ^:integration query-test2
  (t/testing "Query using rql where and field list"
    (let [config (ig/init dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")]
      (t/is (match? {:value "SYSTEM",
                     :isactive true,
                     :ad_org_id 0,
                     :ad_org_id_display "*"}
                    (first (c/query ampere {:ctx ctx
                                                  :table "ad_client"
                                                  :rql-where "eq(ad_client_id,0),eq(isactive,Y)"
                                                  :trx nil
                                                  :fields ["ad_org_id" "isactive" "Value"] 
                                                  })))))))

(t/deftest ^:integration po-crud
  (t/testing "CRUD functionality on PO"
    (let [config (ig/init dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")
          trx nil
          args {:ctx ctx
                :table "test"
                :trx trx
                :data  {:name "testing set values"}}
          po (c/create! ampere args)
          id (:test_id po)]
          (t/is (> id 0M))
          (t/testing "Setting values"
            (let [data {:name "Altered name"
                        :isactive false
                        :t_integer 10
                        :t_amount "120.003"}
                  upo (c/update! ampere (-> args
                                               (assoc :id id)
                                               (assoc :data data)))]
              (t/is (= "Altered name" (:name upo)))))
          (let [read-po (c/get-by-id ampere {:ctx ctx
                                                :table "test"
                                                :trx trx
                                                :id id})]
            (t/is (= id (:test_id read-po)))
            (t/is (= false (:isactive read-po)))
            (t/is (nil? (c/delete! ampere {:ctx ctx
                                              :table "test"
                                              :trx trx
                                              :id id})))
            ))))

(t/deftest ^:integration po-create
  (t/testing "CRUD functionality on PO"
    (let [config (ig/init dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")
          trx nil
          args {:ctx ctx
                :table "test"
                :trx trx 
                :data  {:name "testing set values"}}
          po (c/create! ampere args)
          id (:test_id po)]
          (t/is (> id 0M))
          (t/testing "Setting values"
            (let [data {:name "Altered name"
                        :isactive false
                        :t_integer 10
                        :t_date (u/coerce-to Timestamp "2020-07-01 00:00:00")
                        :t_amount "120.003"}
                  upo (c/update! ampere (-> args
                                               (assoc :id id)
                                               (assoc :data data)))]
              (t/is (= "Altered name" (:name upo)))))
          (let [read-po (c/get-by-id ampere {:ctx ctx
                                                :table "test"
                                                :trx trx
                                                :id id})]
            (t/is (= id (:test_id read-po)))
            (t/is (= false (:isactive read-po)))
            (t/is (nil? (c/delete! ampere {:ctx ctx
                                              :table "test"
                                              :trx trx
                                              :id id})))
            ))))

(t/deftest ^:integration create-invoice
  (t/testing "Create invoice"
    (let [config (ig/init dev-config)
          ampere (::a/ampere config)
          ctx (user/create-user-ctx "system")
          trx nil
          args {:ctx ctx
                :table "c_invoice"
                :trx  nil
                :data {:ad_client_id 11
                       :ad_org_id 11
                       :c_bpartner_id 1014343
                       :c_doctypetarget_id 1000005
                       :m_pricelist_id 1000017
                       :c_bpartner_location_id 1040443
                       :children [{:table "c_invoiceline"
                                   :data {:ad_client_id 1000002
                                          :ad_org_id 1000002}}]}}
          po (c/create! ampere args)
          id (:c_invoice_id po)]
          (t/is (match? {:description nil,
           :posted false,
           :children
           '({:table "c_invoiceline",
             :data
             {:description nil,
              :createdby 0}}),
           :salesrep_id nil,
           :docstatus "DR",
           :processed false,
           :ad_client_id 1000002,
           :grandtotal 0M,
           :updatedby 0,
           :createdby 0,} po))
          )))
