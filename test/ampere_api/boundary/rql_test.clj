(ns ampere-api.boundary.rql-test
  (:require [ampere-api.boundary.rql :as r]
            [clojure.test :as t]
            [java-time :as jt]
            [ring.util.codec :as u])
  (:import java.sql.Timestamp
           java.time.Instant))

(t/deftest parse-rql-query-test
  (let [options {:params [] :fields {"name" String}}]
    (t/testing "Basic statements"
      (t/is (= {:params ["Kill Bill"], :query "name = ?"}
               (r/rql->sql "eq(name,Kill%20Bill)" (atom options))))
      (t/is (= {:params ["Kill Bill"], :query "name = ?"}
               (r/rql->sql "name=eq=Kill%20Bill" (atom options))))
      (t/is (= {:params ["Kill Bill"], :query "name = ?"}
               (r/rql->sql "name=Kill%20Bill" (atom options))))
      (t/is (= {:params ["Kill Bill"], :query "name < ?"}
               (r/rql->sql "lt(name,Kill%20Bill)" (atom options))))
      (t/is (= {:params ["Kill Bill"], :query "name <= ?"}
               (r/rql->sql "le(name,Kill%20Bill)" (atom options))))
      (t/is (= {:params ["Kill Bill"], :query "name > ?"}
               (r/rql->sql "gt(name,Kill%20Bill)" (atom options))))
      (t/is (= {:params ["Kill Bill"], :query "name >= ?"}
               (r/rql->sql "ge(name,Kill%20Bill)" (atom options))))
      (t/is (= {:params ["Kill Bill"], :query "name <> ?"}
               (r/rql->sql "ne(name,Kill%20Bill)" (atom options))))
      (t/is (= {:params ["Kill Bill" 2003], :query "name = ? and year = ?"}
               (r/rql->sql "name=eq=Kill%20Bill&year=eq=2003"
                           (atom {:params []
                                  :fields {"name" String "year" Integer}}))))
      ))
  (t/testing "Query nesting"
    (t/is (= {:params ["a0" "a1" "a2" "a3" "a4"],
              :query "((s0 = ? or s1 = ?) and s2 = ? or s3 = ?) and s4 = ?"}
             (r/rql->sql (str "and(or(and(or(eq(s0,a0),eq(s1,a1)),"
                              "eq(s2,a2)),eq(s3,a3)),eq(s4,a4))")
                         (atom {:params [] :fields {"s0" String "s1" String
                                                    "s2" String "s3" String
                                                    "s4" String}})))))
  (t/testing "Parse query"
    (let  [wc (str "and(eq(Processed,Y),"
                   "or(eq(Docstatus,CO),eq(Docstatus,CL)),"
                   "eq(C_BPartner_ID," 100 "))")]
      (t/is (= {:params ["Y" "CO" "CL" 100],
                :query
                "Processed = ? and (Docstatus = ? or Docstatus = ?) and C_BPartner_ID = ?"}
               (r/rql->sql wc
                           (atom {:params []
                                  :fields {"docstatus" String "processed" String
                                           "c_bpartner_id" Integer}}))
               {:params ["Y" "CO" "CL" 100],
                :query
                "Processed = ? and (Docstatus = ? or Docstatus = ?) and C_BPartner_ID = ?"}))
      (t/is (= {:params [1014343 "n" "CO" "CL"],
                :query
                "c_bpartner_id = ? and processed = ? and (docstatus = ? or docstatus = ?)" }
               (r/rql->sql "eq(c_bpartner_id,1014343)&eq(processed,n)&or(eq(docstatus,CO),eq(docstatus,CL))"
                           (atom {:params []
                                  :fields {"docstatus" String "processed" String
                                           "c_bpartner_id" Integer}}))
               ))
      ))
  )

(t/testing "Nil Query"
  (t/is (= nil
           (r/rql->sql (str "and(or(and(or(eq(s0,a0),eq(s1,a1)),"
                               "eq(s2,a2)),eq(s3,a3)),eq(s4,a4))")
                       nil)))
  (t/is (= nil
           (r/rql->sql nil
                       (atom {:params [] :fields {"s0" String "s1" String
                                                  "s2" String "s3" String
                                                  "s4" String}})))))

(t/deftest parse-date-query
 (t/testing "Test parsing date query, requires url-encoding for date"
   (t/is (= {:params [(jt/instant (jt/with-zone (jt/zoned-date-time 2020 07 1) "UTC"))],
                     :query "created > ?"}
                    (r/rql->sql (str "gt(created,"
                                     (u/url-encode "2020-07-01T00:00:00Z")
                                     ")")
                                (atom {:params []
                                       :fields {"created" Instant}}))))))

(t/deftest q->sqlvec
  (t/is (= ["((s0 = ? or s1 = ?) and s2 = ? or s3 = ?) and s4 = ?"          
            "a0"
            "a1"
            "a2"
            "a3"
            "a4"]
           (r/q->sqlvec (r/rql->sql (str "and(or(and(or(eq(s0,a0),eq(s1,a1)),"
                                        "eq(s2,a2)),eq(s3,a3)),eq(s4,a4))")
                                   (atom {:params [] :fields {"s0" String "s1" String
                                                              "s2" String "s3" String
                                                              "s4" String}})))))
  )

(t/deftest nil-q->sqlvec
  (t/is (= nil
           (r/q->sqlvec nil))))
