(ns ampere-api.boundary.util-test
  (:require [ampere-api.boundary.util :as u]
            [clojure.test :as t]))

(t/deftest coercion
  (t/testing "coerce values"
    (t/is (instance? Integer (u/coerce-to Integer 1)))
    (t/is (instance? Integer (u/coerce-to Integer "1")))
    (t/is (instance? Integer (u/coerce-to Integer 1M)))
    (t/is (instance? Integer (u/coerce-to Integer 1.3)))
    (t/is (thrown? ClassCastException (u/coerce-to Integer false)))
    (t/is (instance? BigDecimal (u/coerce-to BigDecimal 1)))
    (t/is (instance? BigDecimal (u/coerce-to BigDecimal "1")))
    (t/is (instance? BigDecimal (u/coerce-to BigDecimal 1M)))
    (t/is (instance? BigDecimal (u/coerce-to BigDecimal 1.3)))
    (t/is (thrown? IllegalArgumentException (u/coerce-to BigDecimal false)))
    (t/is (u/coerce-to Boolean true))
    (t/is (u/coerce-to Boolean "Y"))
    (t/is (u/coerce-to Boolean "true"))
    (t/is (not (u/coerce-to Boolean false)))
    (t/is (not (u/coerce-to Boolean "false")))
    (t/is (not (u/coerce-to Boolean "anything else")))
    (t/is (not (u/coerce-to Boolean nil)))
    ))
