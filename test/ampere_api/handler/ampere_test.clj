(ns ampere-api.handler.ampere-test
  (:require [ampere-api.boundary.ampere :as b]
            [ampere-api.handler.ampere :as a]
            [clojure.test :as t]
            [integrant.core :as ig]
            [ring.mock.request :as mock]
            [shrubbery.core :as s]
            [ampere-api.boundary.core :as c]))

(def mock-ampere
  (s/mock c/Model
          {:create! {:new :po}
           :get-by-id {:test_id 11
                          :name "Test PO"}
           :query {:test_id 11
                  :name "Test PO"}}))

(t/deftest get-handler-ok
  (t/testing "Get handler"
    (let [handler (ig/init-key ::a/get {:ampere mock-ampere})
          response (handler (assoc (mock/request :get "/not-used")
                                   :ataraxy/result [:get "test" 11 {:fields ["name"]}]
                                   :ctx {}))]
      (t/is (= [:ataraxy.response/ok {:result {:test_id 11, :name "Test PO"}}] response)))))

(t/deftest get-handler-list
  (t/testing "Get handler for list"
    (let [handler (ig/init-key ::a/list {:ampere mock-ampere})
          response (handler (assoc (mock/request :get "/not-used")
                                   :ataraxy/result [:ampere-api.handler.ampere/list
                                                    "c_order"
                                                    {"where"
                                                     "and(Processed=Y,or(Docstatus=CO,
Docstatus=CL),C_BPartner_ID=100)"}]
                                   :ctx {}))]
      (t/is (= [:ataraxy.response/ok {:result {:test_id 11, :name "Test PO"}}] response))
      (t/is (s/calls mock-ampere)))))

(t/deftest get-handler-bad-request
  (t/testing "Get handler"
    (let [handler (ig/init-key ::a/get {:ampere (s/stub c/Model)})
          response (handler (assoc (mock/request :get "/not-used")
                                             :ctx {}))]
      (t/is (= :ataraxy.response/bad-request (first response))))))

(t/deftest get-handler-not-found
  (t/testing "Get handler"
    (let [handler (ig/init-key ::a/get {:ampere (s/stub c/Model
                                                        {:get-po-by-id nil})})
          response (handler (assoc (mock/request :get "/not-used")
                                   :ataraxy/result [:get "test" 11 {:fields ["name"]}]
                                   :ctx {}))]
      (t/is (= [:ataraxy.response/not-found {:error :not-found}] response)))))
