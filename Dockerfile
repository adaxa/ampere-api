FROM openjdk:11.0.8-jre-slim-buster

WORKDIR /

COPY target/ampere-api-0.3.3-standalone.jar app.jar
EXPOSE 3030

ENTRYPOINT ["java", "-jar", "app.jar"]
