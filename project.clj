(defproject ampere-api "0.3.3"
  :description "A REST-style API for interfacing with Ampere"
  :url "https://gitlab.com/adaxa/ampere-api"
  :min-lein-version "2.0.0"
  :repositories {"gitlab-ampere" "https://gitlab.com/api/v4/projects/11127333/packages/maven"
                 "releases"  {:url           "gitlab://gitlab.com/api/v4/projects/20324179/packages/maven"
                              :username      "Job-Token"
                              :password      :env/ci_job_token
                              :sign-releases false}

                 "snapshots" {:url          "gitlab://gitlab.com/api/v4/projects/20324179/packages/maven"
                              :username      "Job-Token"
                              :password      :env/ci_job_token}}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [buddy/buddy-core "1.6.0"]
                 [buddy/buddy-sign "3.1.0"]
                 [diehard "0.9.2"]
                 [duct/logger "0.3.0"]
                 [http-kit "2.3.0"]
                 [integrant "0.8.0"]
                 [org.clojure/core.cache "0.8.2"]
                 [org.clojure/data.json "0.2.7"]
                 [com.fasterxml.jackson.core/jackson-core "2.10.4"]
                 [uk.me.rkd.ttlcache "0.1.0"]
                 [duct/core "0.7.0"]
                 [clj-time "0.14.3"]
                 [clojure.java-time "0.3.2"]
                 [duct/module.ataraxy "0.3.0"]
                 [duct/module.logging "0.4.0"]
                 [duct/module.sql "0.5.0"]
                 [duct/module.web "0.7.0"]
		 [com.layerware/hugsql "0.5.1"]
                 [duct/middleware.buddy "0.1.0"]
                 [yogthos/config "1.1.7"]
                 [ring-cors "0.1.13"]
                 [adaxa/ampere "0.3.1"]
                 [coercer "0.2.0"]
                 [com.layerware/hugsql-core "0.5.1"]
                 [com.layerware/hugsql-adapter-next-jdbc "0.5.1"]
                 [com.github.seancorfield/next.jdbc "1.2.659"]
                 [seancorfield/next.jdbc "1.0.13"]
                 [org.postgresql/postgresql "42.2.20"]
                 [com.gearswithingears/shrubbery "0.4.1"]
                 [instaparse "1.4.10"]
                 [ring/ring-codec "1.1.2"]
                 [nubank/matcher-combinators "3.1.4"]]
  :plugins [[duct/lein-duct "0.12.1"]
            [lein-marginalia "0.9.1"]
            [nicheware/lein-gitlab-wagon "1.0.0"]]
  :main ^:skip-aot ampere-api.main
  :resource-paths ["resources" "target/resources"]
  :prep-tasks     ["javac" "compile" ["run" ":duct/compiler"]]
  :middleware     [lein-duct.plugin/middleware]
  :test-selectors {:default (complement :integration) 
                   :integration :integration} 
  :profiles
  {:dev  [:project/dev :profiles/dev]
   :repl {:prep-tasks   ^:replace ["javac" "compile"]
          :repl-options {:init-ns user}}
   :uberjar {:aot :all}
   :profiles/dev {}
   :project/dev  {:source-paths   ["dev/src"]
                  :resource-paths ["dev/resources"]
                  :dependencies   [[integrant/repl "0.3.1"]
                                   [eftest "0.5.7"]
                                   [kerodon "0.9.0"]]}
   :cicd {:local-repo ".m2/repository"}})
