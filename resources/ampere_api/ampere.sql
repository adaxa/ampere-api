-- :name get-table-data-json :? :1
-- :doc get data from table as json
SELECT array_to_json(array_agg(row_to_json(i, true))) as result
  FROM (:snip:sql) i

-- :name get-csv :? :1
-- :doc get data from table as csv file
 COPY (:snip:sql)
TO STDOUT
       WITH (FORMAT CSV, HEADER)
